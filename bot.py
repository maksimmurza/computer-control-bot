#!/usr/bin/python

import config
import psutil
import telebot
from telebot import types
import os

bot = telebot.TeleBot(config.token)

keyboard = types.ReplyKeyboardMarkup()
keyboard.row('quiet', 'mute', 'louder')
keyboard.row('light dn', 'light', 'light up')
keyboard.row('cmd', 'inf', 'scr', 'trn')
keyboard.row('lock', 'off', 'restart')

@bot.message_handler(content_types=['text'])
def handler(message):
	if message.text == 'quiet':
		os.system("pactl set-sink-volume 0 -3%")
	elif message.text == 'mute':
		os.system("pactl set-sink-mute 0 toggle")
	elif message.text == 'louder':
		os.system("pactl set-sink-volume 0 +3%")
	elif message.text == 'light dn':
		os.system("light -U 10")
	elif message.text == 'light':
		os.system("light -S 0")
	elif message.text == 'light up':
		os.system("light -A 10")
	#elif message.text == 'lock':
	#	os.system("gnome-session-quit --logout --force")
	#elif message.text == 'off':
	#	os.system("echo " + config.password + " | sudo -S off")
	#elif message.text == 'restart':
	#	os.system("echo " + config.password + " | sudo -S reboot")
	elif message.text == 'scr':
		os.system("gnome-screenshot --file=" + config.screen_path)
		photo = open(config.screen_path, 'rb')
		bot.send_photo(config.user_id, photo)
	elif message.text == 'inf':

	#Uptime
		info = open(config.info_path, 'w')
		list = os.popen('uptime | cut -c 15-18').readlines()
		buf = 'Uptime: ' + list[0] + '\n'
		info.write(buf)

	#CPU
		buf = 'CPU: ' + str(psutil.cpu_percent()) + '%\n'
		info.write(buf)

	#RAM
		buf = 'RAM: ' + str(round(psutil.virtual_memory().active/psutil.virtual_memory().total, 2) * 100) + '%\n\n'
		info.write(buf)

	#Volume
		list = os.popen("amixer | grep 'Front Left: Playback'").readlines()
		string = list[0]
		if string[-11] != '[':
			volume = string[-11:-8]
		else:
			volume = string[-10:-8]
		buf = 'Volume: ' + volume + '%\n'
		info.write(buf)

	#Brightness
		list = os.popen('light -G').readlines()
		string = list[0]
		buf = 'Brightness: ' + string[0:-2] + '%\n'
		info.write(buf)

		info = open(config.info_path, 'r')
		info_str = info.read()
		bot.send_message(config.user_id, info_str)
		
@bot.message_handler(commands=['start'])
def password(message):
	bot.send_message(config.user_id, 'Welcome!')
	
@bot.message_handler(commands=['help'])
def help(message):
	bot.send_message(config.user_id, 'This bot is for administration PC')

if __name__ == '__main__':
	bot.send_message(config.user_id, 'Command:', reply_markup=keyboard)
	bot.polling()